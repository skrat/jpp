# jpp

browserify v2 plugin for cpp like preprocessing

# example

foo.coffee:

``` coffee
console.log("here I am")
#ifdef DEBUG
console.log("DEBUG: post here I am")
#endif
```

now when you use browserify command, or your own build script, and you include
`--DDEBUG` on the command line, two line will be logged. If you don't include
this argument on the command line, one line will be printed.
