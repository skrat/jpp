var through = require('through');

var IS_IFDEF = /^\s*#ifdef ([A-Za-z_][A-Za-z_0-9]*)$/;
var IS_ELSE  = /^\s*#else$/;
var IS_ENDIF = /^\s*#endif$/;

function preprocess(file, data, callback) {
    var defines = {},
        processed = [],
        lines = data.split('\n');

    process.argv.forEach(function(arg) {
        var name,
            value,
            parts;
        if (arg.substr(0, 3) === '--D') {
            parts = arg.substr(3).split('=');
            name = parts[0];
            if (parts.length > 1) {
                value = parts[1];
            } else {
                value = true;
            }
            defines[name] = value;
        }
    });

    var isIn_IfDef = false,
        isIn_Else = false,
        match,
        def;

    lines.forEach(function(line) {
        match = line.match(IS_IFDEF);
        if (match) {
            isIn_IfDef = true;
            def = match[1];
            return;
        }
        if (line.match(IS_ELSE) && isIn_IfDef) {
            isIn_Else = true;
            return;
        }
        if (line.match(IS_ENDIF)) {
            isIn_IfDef = false;
            isIn_Else = false;
            return;
        }
        if (isIn_IfDef) {
            if (!defines[def] && !isIn_Else)
                return;
            if (defines[def] && isIn_Else)
                return;
        }
        processed.push(line);
    });

    callback(null, processed.join('\n'));
}

function jpp(file) {

    var data   = '',
        stream = through(write, end);

    return stream;

    function write(buf) {
        data += buf;
    }

    function end() {
        preprocess(file, data, function(error, result) {
            if (error) stream.emit('error', error);
            stream.queue(result);
            stream.queue(null);
        });
    }
}

jpp.preprocess = preprocess;
module.exports = jpp;
